University of Dayton

Department of Computer Science

CPS 490 - Spring 2020

Dr. Phu Phung

Capstone II Proposal
Indicator weights/heat scores - GE Aviation
Team members
Brian Taylor, taylorb1@udayton.edu
Robert Stevenson, stevensonr2@udayton
Jon Henry, henryj14@udayton.edu

Company Mentors
Eric Gero, GE - Aviation

GE - Aviation

River Park Drive, Dayton, Ohio 45479

Project homepage
https://trello.com/b/zyP2VdJG/capstone2

Overview
Describe the overview of the project with a high-level architecture figure (See an example below).

Overview Architecture

Figure 1. - A Sample of Overview Architecture of the proposed project.

Project Context and Scope
Describe the context where the project will be used or deployed and the scope of the project your team will develop

High-level Requirements
List high-level requirements of the project that your team will develop into use cases in later steps

Technology
Specify the development approach of your team, including programming languages, database, development, testing, and deployment environments.

You also need to mention if there are any technical support and preferences from the company and if they provide an existing version of the application.

Project Management
We will follow the Scrum approach, thus your team needs to decide sprint cycles, team meeting schedules, including Fall 2019, and Spring 2020.

Provide the link to your team bitbucket repository and Trello board. You must share the repo and the board with the instructor.

Company Support
Based on your discussion with the company, describe here what they will support your team during Fall 2019, and Spring 2020, and the communication plan with the company mentors